#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

class QMenu;

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:

private:
    Ui::Widget *ui;
    QMenu *menu;
};
#endif // WIDGET_H

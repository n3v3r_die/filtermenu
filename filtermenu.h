#ifndef FILTERMENU_H
#define FILTERMENU_H
#include <QMenu>
#include <QStyledItemDelegate>

class QLineEdit;
class QListView;
class ActionModel;
class QWidgetAction;
class QSortFilterProxyModel;

class FilterBox: public QWidget{
public:
    FilterBox(QWidget *parent = nullptr);
    QLineEdit *filterLineEdit() const;
    void showFilterView(bool show);
    void setMaxSize(const QSize size);
    void fillFilterView(QMenu *menu);

private:
    QLineEdit *mFilterLineEdit;
    QListView *mFilterListView;
    ActionModel *m_model;
    QSortFilterProxyModel *mProxyModel;
    void setupFilterWidget();
};

class FilterViewDelegate : public QStyledItemDelegate{
public:
    FilterViewDelegate(QObject *obj = nullptr, int height = 18);
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;
private:
    int m_height;
};

class FilterMenu : public QMenu
{
public:
    FilterMenu(QWidget *parent = nullptr);
    void showFilterView();
    void setFilterIcon(QIcon icon);
    void setPlaceHolderText(const QString text);
private:
    FilterBox *mFilterBox;
    QWidgetAction *mActionFilterBox;
    void showHideMenuEntries(const bool show);
};

#endif // FILTERMENU_H

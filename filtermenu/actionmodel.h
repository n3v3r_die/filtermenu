#ifndef ACTIONMODEL_H
#define ACTIONMODEL_H

#include <QAbstractListModel>

class QMenu;
class QAction;

class ActionModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit ActionModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    void fillActions(QMenu *menu);
    void executeAction(const QModelIndex &index);

private:
    QList<QAction*> mActions;
    void fillActionsRecursive(QMenu *menu);
    bool existsAction(const QAction *action);
};

#endif // ACTIONMODEL_H

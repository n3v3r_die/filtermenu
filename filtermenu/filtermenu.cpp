#include "filtermenu.h"
#include "actionmodel.h"
#include <QLineEdit>
#include <QListView>
#include <QLayout>
#include <QWidgetAction>
#include <QSortFilterProxyModel>

FilterMenu::FilterMenu(QWidget *parent):
    QMenu(parent),
    mFilterBox(new FilterBox(this)),
    mActionFilterBox(new QWidgetAction(this))
{
    mActionFilterBox->setDefaultWidget(mFilterBox);
}

void FilterMenu::showFilterView()
{
    addSeparator();
    addAction(mActionFilterBox);
    mFilterBox->setMaxSize(sizeHint());
    mFilterBox->fillFilterView(this);

    connect(this, &QMenu::aboutToShow, [=](){
        mFilterBox->filterLineEdit()->setFocus();
    });
    connect(mFilterBox->filterLineEdit(), &QLineEdit::textChanged, [=] (){
        showHideMenuEntries(mFilterBox->filterLineEdit()->text().isEmpty());
    });
}

void FilterMenu::setFilterIcon(QIcon icon)
{
    mFilterBox->filterLineEdit()->addAction(icon, QLineEdit::LeadingPosition);
}

void FilterMenu::setPlaceHolderText(const QString text)
{
    if (auto lineEdit = mFilterBox->filterLineEdit())
        lineEdit->setPlaceholderText(text);
}

void FilterMenu::showHideMenuEntries(const bool show)
{
    mFilterBox->showFilterView(!show);

    auto const actions = this->actions();
    for (auto const &act : actions){
        if (qobject_cast<QWidgetAction *>(act) == nullptr)
            act->setVisible(show);
    }
}

FilterBox::FilterBox(QWidget *parent):
    QWidget(parent),
    mFilterLineEdit(new QLineEdit(this)),
    mFilterListView(new QListView(this)),
    m_model(new ActionModel(this)),
    mProxyModel(new QSortFilterProxyModel(this))
{
    setLayout(new QVBoxLayout());
    layout()->addWidget(mFilterListView);
    layout()->addWidget(mFilterLineEdit);
    setupFilterWidget();
}

QLineEdit *FilterBox::filterLineEdit() const
{
    return mFilterLineEdit;
}

void FilterBox::showFilterView(bool show)
{
    mFilterListView->setVisible(show);
}

void FilterBox::setMaxSize(const QSize size)
{
    const auto spacing = layout()->spacing();
    setMaximumSize(size.width(), size.height()- spacing);
}

void FilterBox::fillFilterView(QMenu *menu)
{
    m_model->fillActions(menu);
    mProxyModel->setSourceModel(m_model);
}

void FilterBox::setupFilterWidget()
{
    layout()->setMargin(0);
    mFilterLineEdit->setClearButtonEnabled(true);
    mFilterListView->setVisible(false);
    mFilterListView->setFrameStyle(QFrame::NoFrame);
    mFilterListView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    mFilterListView->setItemDelegate(new FilterViewDelegate(this, mFilterLineEdit->height()));
    mFilterListView->viewport()->setAutoFillBackground(false);
    mFilterListView->setModel(mProxyModel);
    mProxyModel->setFilterCaseSensitivity(Qt::CaseInsensitive);
    connect(mFilterLineEdit, &QLineEdit::textChanged, [=] (){
        mProxyModel->setFilterFixedString(mFilterLineEdit->text());
    });
    connect(mFilterListView, &QListView::activated, [=] (const QModelIndex &index){
        auto ind = mProxyModel->mapToSource(index);
        m_model->executeAction(ind);
    });
}

FilterViewDelegate::FilterViewDelegate(QObject *obj, int height):
    QStyledItemDelegate(obj),
    m_height(height)
{}

QSize FilterViewDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &/*index*/) const
{
    return QSize(option.rect.width(), m_height);
}

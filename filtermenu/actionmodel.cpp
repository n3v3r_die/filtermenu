#include "actionmodel.h"
#include <QMenu>
#include <QAction>
#include <QDebug>

ActionModel::ActionModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

int ActionModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return mActions.size();
}

QVariant ActionModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() < 0 || index.row() >= mActions.size()){
        return  QVariant();
    }

    auto action = mActions.at(index.row());
    switch (role) {
        case Qt::DisplayRole:
            return action->text();
        case Qt::ToolTipRole:
            return action->toolTip();
        case Qt::DecorationRole:
            return action->icon();
    }
    return QVariant();
}

void ActionModel::fillActions(QMenu *menu)
{
    mActions.clear();
    fillActionsRecursive(menu);
}

void ActionModel::executeAction(const QModelIndex &index)
{
    if (!index.isValid() || index.row() >= mActions.size())
        return;
    mActions.at(index.row())->trigger();
}

void ActionModel::fillActionsRecursive(QMenu *menu)
{
    for (auto const &act: menu->actions()){
        if (QMenu * subMenu = act->menu()){
            fillActionsRecursive(subMenu);
        }else if (existsAction(act)){
            mActions.append(act);
        }
    }
}

bool ActionModel::existsAction(const QAction *action)
{
    return !(action->isWidgetType() || action->isSeparator());
}

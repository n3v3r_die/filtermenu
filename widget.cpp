#include "widget.h"
#include "ui_widget.h"
#include <QMenu>
#include <QAction>
#include "filtermenu/filtermenu.h"
#include <QMessageBox>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
    , menu(new QMenu(this))
{
    ui->setupUi(this);

    auto filterMenu = new FilterMenu(this);

    auto subMenu = filterMenu->addMenu(QIcon(":/icons/12"), "submenu 1");
    subMenu->addAction(QIcon(":/icons/1"), "Action 1");
    subMenu->addAction(QIcon(":/icons/2"), "Action 2");
    subMenu->addAction(QIcon(":/icons/3"), "Action 3");
    subMenu->addAction(QIcon(":/icons/4"), "Action 4");
    subMenu->addAction(QIcon(":/icons/5"), "Action 5");
    subMenu = filterMenu->addMenu(QIcon(":/icons/6"), "submenu 2");
    subMenu->addAction(QIcon(":/icons/7"), "Action 1");
    subMenu->addAction(QIcon(":/icons/8"), "Action 2");
    subMenu->addAction(QIcon(":/icons/9"), "Action 3");
    subMenu->addAction(QIcon(":/icons/10"), "Action 4");
    subMenu->addAction(QIcon(":/icons/11"), "Action 5");

    filterMenu->addAction(QIcon(":/icons/12"), "Action 1");
    filterMenu->addAction(QIcon(":/icons/13"), "Action 2");
    filterMenu->addAction(QIcon(":/icons/1"), "Action 3");
    filterMenu->addAction(QIcon(":/icons/2"), "Action 4");
    filterMenu->showFilterView();
    filterMenu->setPlaceHolderText("Search...");
    filterMenu->setFilterIcon(QIcon(":/icons/search"));
    ui->pushButton_2->setMenu(filterMenu);
}

Widget::~Widget()
{
    delete ui;
}


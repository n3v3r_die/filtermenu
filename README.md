# FilterMenu

A Simple Qt widget extends QMenu with filter field for searching actions in menu.

#Create menu object
`auto filterMenu = new FilterMenu(this);`

#Add some action or menus
`subMenu->addAction(QIcon(":/icons/1"), "Action 1");`

#Create filterbox 
`filterMenu->showFilterView();`

#Set a PlaceHolderText
`filterMenu->setPlaceHolderText("Search...");`

#Set icon
`filterMenu->setFilterIcon(QIcon(":/icons/search"));`

![Preview](https://gitlab.com/n3v3r_die/filtermenu/-/blob/master/preview.png?raw=true "Preview")
